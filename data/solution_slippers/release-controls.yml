title: Release controls
description: Implement controls to ensure teams ship high quality and secure code
canonical_path: "/solutions/release-controls/"
file_name: release-controls
twitter_image: /images/opengraph/open-source-card.png
header:
  image: /images/icons/explore-open-source.svg
  subtitle: Implement controls to ensure teams ship high quality and secure code
  title: Release controls
  buttons:
    - button_url: /free-trial/
      button_text: Try GitLab Free
body_image:
  sections:
    - heading: Code Controls
      image: /images/solutions/approval-rules.png
      type: image-left
      body: >-
        Ensure that code follows your company's policies
        

        *   Enforce code review approvals via [approval rules for code review.](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)
        
        *   Reject code that does not comply to company policy with [push rules.](https://docs.gitlab.com/ee/push_rules/push_rules.html)
        
    - heading: Merge Controls
      image: /images/solutions/mr-dependencies.png
      type: image-right
      body: >-
        Ensure that only code with adequate approvals is merged
  
        *   Sequence and coordinate merge requests with [dependencies.](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_dependencies.html)
        

        *   Enforce necessary approvals with [required merge request approvals.](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
    - heading: Access Controls
      image: /images/solutions/protected-access.png
      type: image-left
      body: >-
        Ensure only the right individuals with the right permissions can make changes to specific code / environments.


        *   Restrict users, groups, accounts allowed to deploy to sensitive environments with [protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html).
        
        *   Restrict / [lock new members](https://docs.gitlab.com/ee/user/group/index.html#member-lock) from being added to a project within a group.
        
        *   [Restrict push/merge access](https://docs.gitlab.com/ee/user/project/protected_branches.html#restricting-push-and-merge-access-to-certain-users) to a protected branch
videos: 
  heading: Featured Videos
  type: Code Review Carousel
cta_banner:
  - title: Free GitLab trial
    subtitle: GitLab is more than just source code management or CI/CD. It is a full software development lifecycle & DevOps tool in a single application.
    buttons:
      - button_url: /free-trial
        button_text: Start your free trial
      - button_url: /demo/
        button_text: Watch a demo


