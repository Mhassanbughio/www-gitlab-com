---
layout: job_family_page
title: "Tax"
---

## Levels

| Role                                                | Grade |
| ----- | ----- | 
| VP, Tax | Level 12 |
| Director, Tax(Domestic/International) | Level 10 |
| Manager, Tax (Domestic/International) | Level 8 |
| Sr Analyst | Level 7 |
| Analyst | Level 6 |
| Jr Analyst | Level 5 |

### Junior Tax Analyst

The Junior Tax Analyst reports to the Manager, Tax. The responsibilities and requirements for a Junior Tax Analyst would be defined at the time of business need for the role. 

#### Junior Tax Analyst Job Grade

The Junior Tax Analyst is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Tax Analyst (Intermediate) 

The Tax Analyst (Intermediate) reports to the Manager, Tax. The responsibilities and requirements for a Tax Analyst would be defined at the time of business need for the role. 

#### Tax Analyst (Intermediate) Job Grade

The Tax Analyst (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Senior Tax Analyst

The Senior Tax Analyst resorts to the Manager, Tax.

#### Senior Tax Analyst Job Grade

The Senior Tax Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Tax Analyst Responsibilities

* Manage indirect tax audits, including gathering information and responding to audit requests
* Prepare monthly sales and use tax-related journal entries and ensure that all sales and use tax liabilities and payments are recorded properly
* Work with team to create new processes and develop efficiencies in the tax process based on specialty 
* Research, analyze, and model the impacts of legislative changes based on specialty 
* Prepare/Review property tax returns
* Prepare and ensure that all annual reports are filed timely
* Ensure monthly accrual and reconciliation of indirect tax accounts as well as the ASC 450 process and ongoing analysis.
* Manage the business license renewal process and perform necessary research to determine if any new filings are required
* Review tax requirements for new products and/or system implementations based on specialty 
* Partner with various teams, including accounting, legal, engineering, operations, and finance on a variety of initiatives
* Design and maintain SOX control processes and evidences 
* Partner with and manage outside service providers on tax compliance based on specialty 
* Assist with miscellaneous projects as necessary

#### Senior Tax Analyst Requirements

* Bachelor’s Degree (B.S.) in Accounting, tax or related field.
* CPA designation desired.
* Progressive, relevant experience based on tax specialty. 
* Experience with Software and/or SAAS in a high growth environment.
* Taking initiative and also working as a team player
* Experience with NetSuite, SalesForce, Zuora, Coupa, GSuite and Avalara are plusses.
* Superior Excel and analytical skills
* Outstanding organizational and presentation skills and attention to detail
* The capacity to multitask and prioritize workload
* Discretion and respect for confidential information
* Ability to use GitLab

### Staff Tax Analyst

The Staff Tax Analyst reports to the Manager, Tax. 

#### Staff Tax Analyst Job Grade

The Staff Tax Analyst  is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff Tax Analyst Responsibilities

* Extends that of the Senior Tax Analyst responsibilities

#### Staff Tax Analyst Requirements

* Strong business judgment applied to tax and finance operational activities
* Demonstrated experience in mentoring and leading a distributed team
* Leadership experience in a software or global technology company
* Proven strategic and tactical vision to lead a high performing team
 
### Manager, Tax

The Manager, Tax reports to the Director, Tax.

#### Manager, Tax Job Grade

The Manager, Tax is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Tax Responsibilities

* Extends that of the Staff Tax Analyst responsibilities
* Manage a team of analysts
* Conduct recurring 1:1’s and evaluate team’s performance 

#### Manager, Tax Requirements

* Extends that of the Staff Tax Analyst requirement 
* Demonstration of past mentorship and leadership capabilities

### Director, Tax

The Director, Tax reports to the [VP, Tax](/job-families/finance/vice-president-of-tax/).

#### Director, Tax Job Grade

The Director, Tax  is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Tax Responsibilities

* Staff mentoring and supervision
* SOX control implementation, testing, and narrative writing
* Process implementation for global income tax accounting
* Interaction with Big 4 financial statement auditors
* Working with outside service providers on projects including but not limited to:
* Income tax compliance
* Sales tax compliance
* Maintenance of a compliance calendar
* Tax audit supervision, both direct and indirect taxes
* M&A and purchase accounting (income and indirect taxes)
* Tax account reconciliation
* Working with Finance team members on intercompany settlements

#### Director, Tax Requirements

* Bachelor’s Degree (B.S.) in Accounting. Master’s Degree in Business Taxation preferred.
* JD and/or CPA preferred.
* Experience with Software and/or SAAS in a high growth environment.
* Technical understanding of ASC 740, ASC 718, and ASC 805 for global operations
* Spreadsheet modeling for ASC 740, tax return workpapers, and project planning
* Tax and tax provision issues relating to stock based compensation, including contra-DTAs produced by Sec. 162M
* Some working knowledge of VAT/GST, transfer pricing, and US international taxes including FDII, GILTI, BEAT, FTCs and Subpart F 
* Project management
* Audit supervision
* Memo writing
* Taking initiative and also working as a team player
* Experience with NetSuite, SalesForce, Zuora, Coupa, GSuite and Avalara are plusses.
* Ability to use GitLab

## Specialties

### Domestic

* Primary focus on US direct and/or indirect tax
* Assist with the resolution of all state notices 
* Maintain a U.S. indirect tax compliance calendar
* Prepare/Review all aspects of U.S. indirect tax filings including, but not limited to, sales and use tax and gross receipt tax
* Assist with the resolution of all state notices relating to US indirect tax
* 3+ years indirect tax experience with multiple states and local jurisdictions preferred.

### International

* Primary focus on international tax 
* Working knowledge of multiple International tax areas
* Proven international experience working with overseas operations
* Proven success with managing tax on a global scale

## Performance Indicators

* [Effective Tax Rate](https://about.gitlab.com/handbook/tax/performance-indicators/#effective-tax-rate-etr)
* [Budget vs. Actual](https://about.gitlab.com/handbook/tax/performance-indicators/#budget-vs-actual)
* [Audit Adjustments](https://about.gitlab.com/handbook/tax/performance-indicators/#audit-adjustments)
* [International Expansion](https://about.gitlab.com/handbook/tax/performance-indicators/#international-expansion)

## Career Ladder

The Tax job family career ladder is outlined on this page. 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
 
* Next, qualified candidates will be invited to schedule a first interview with the Hiring Manager
* Candidates will then be invited to schedule an interview with 2 - 4 team members
* Finally, candidates may be invited to a final executive interview
 
Additional details about our process can be found on our [hiring page](/handbook/hiring/)
